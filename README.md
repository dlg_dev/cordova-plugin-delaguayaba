## deLaGuayaba Ionic required changes to Info.plist file for web app development

**Cordova / PhoneGap Plugin to allow required deLaGuayaba changes to Info.plist file**

## Install

#### Latest published version is 0.0.1

```
cordova plugin add https://deLaGuayaba@bitbucket.org/dlg_dev/cordova-plugin-delaguayaba.git

```

## Platforms

Applies to iOS (9+) only and Android.

## License

[MIT License](http://ilee.mit-license.org)

## Contact

[deLaGuayaba](http://delaguayaba.com)